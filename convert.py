#!/usr/bin/env python2
#coding=utf-8

import sys
import logging
from zipfile import is_zipfile, ZipFile
import platform

from bs4 import BeautifulSoup
import bs4.element


class Converter(object):
    """
    basic convert
    """

    def __init__(self):
        self.system = platform.system()
        if self.system.startswith('Linux'):
            self.newline = '\n' * 2
        elif self.system.startswith('Win') or self.system.startswith('win'):
            self.newline = '\r\n' * 2
        else:
            self.newline = '\r' * 2

        self.tag = None

    def convert(self, tag):
        raise NotImplementedError("Not Implemented")

    # Depth First Traversal
    def _tag_handle(self):
        if self.tag is None:
            raise TypeError("tag is NoneType")
        for child in self.tag.descendants:
            tag_name = getattr(child, 'name')
            if tag_name == 'a':
                self._hyperlink_handler(child)
            elif tag_name == 'b':
                self._bold_handler(child)
            elif tag_name == 'i':
                self._italic_handler(child)
            elif tag_name is None:
                pass
        markdown = "".join([child for child in self.tag.strings])
        return markdown

    @staticmethod
    def _bold_handler(tag):
        # return u"**%s**" % tag.text
        tag.string.replace_with("**%s**" % tag.string)

    @staticmethod
    def _hyperlink_handler(tag):
        # return u"[%s](%s)" % (tag.text, tag.attrs.get('href'))
        tag.string.replace_with("[%s](%s)" % (tag.string, tag.attrs.get('href')))

    @staticmethod
    def _italic_handler(tag):
        # return u"*%s*" % tag.text
        tag.string.replace_with("*%s*" % tag.string)


class PConverter(Converter):
    def __init__(self):
        super(PConverter, self).__init__()

    def convert(self, tag):
        self.tag = tag
        return "%s%s" % (self._tag_handle(), self.newline)


class BlockquoteConverter(Converter):
    def __init__(self):
        super(BlockquoteConverter, self).__init__()

    def convert(self, tag):
        self.tag = tag
        return "> %s%s" % (self._tag_handle(), self.newline)


class HeadingConverter(Converter):
    def __init__(self):
        super(HeadingConverter, self).__init__()

    def convert(self, tag):
        self.tag = tag
        # In medium, h1 is equal to h2, h2 is equal to h3
        if self.tag.name == 'h2':
            return "# %s%s" % (self._tag_handle(), self.newline)
        elif self.tag.name == 'h3':
            return "## %s%s" % (self._tag_handle(), self.newline)


class ListConverter(Converter):
    def __init__(self):
        super(ListConverter, self).__init__()

    def convert(self, tag):
        self.tag = tag
        if self.tag.name in ('ol', 'ul'):
            return self._tag_handle()

    def _tag_handle(self):
        if self.tag is None:
            raise TypeError("tag is NoneType")
        parent_tag_name = self.tag.name
        num = 0
        for child in self.tag.descendants:
            tag_name = getattr(child, 'name')
            if tag_name == 'li':
                num += 1
                self._li_handler(child, parent_tag_name, num)
            elif tag_name == 'a':
                self._hyperlink_handler(child)
            elif tag_name == 'b':
                self._bold_handler(child)
            elif tag_name == 'i':
                self._italic_handler(child)
            elif tag_name is None:
                continue
        markdown = "".join([child for child in self.tag.strings])
        return markdown

    def _li_handler(self, tag, parent_tag_name, num):
        if parent_tag_name == 'ul':
            tag.string.replace_with("- %s%s" % (tag.string, self.newline))
        elif parent_tag_name == 'ol':
            tag.string.replace_with("%d. %s%s" % (num, tag.string, self.newline))


class ImgConverter(Converter):
    def __init__(self):
        super(ImgConverter, self).__init__()

    def convert(self, tag):
        self.tag = tag
        src = ''
        desc = ''
        for i in self.tag:
            tag_name = getattr(i, 'name')
            if tag_name == 'img':
                src = i.attrs.get('src')
            elif tag_name == 'figcaption':
                desc = i.get_text()
        if desc:
            # Not a good way to show <figcaption>
            return '![%s](%s "%s")%s' % (desc, src, desc, self.newline)
        else:
            return '![](%s)%s' % (src, self.newline)


class SubtitleConverter(Converter):
    def __init__(self):
        super(SubtitleConverter, self).__init__()
        self.system = platform.system()
        if self.system.startswith('Linux'):
            self.newline = '\n'
        elif self.system.startswith('Win') or self.system.startswith('win'):
            self.newline = '\r\n'
        else:
            self.newline = '\r'

    def convert(self, tag):
        # If you don't want to Convert subtitle, please comment it and uncomment next line.
        # return ""
        return "%s%s" % (tag.string.strip('\r\n'), self.newline)
        # It seems that the dashes we need is contain by <div class="section-divider layout-singlecolumn">
        # So I removed it temporary till find a way to fix it.
        # return "%s%s------%s" % (tag.string.strip('\r\n'), self.newline, self.newline)


class Factory(object):
    def __init__(self):
        self.soup = None
        self.title = None
        self.content = None
        self.stc = SubtitleConverter()
        self.pc = PConverter()
        self.bqc = BlockquoteConverter()
        self.hc = HeadingConverter()
        self.lc = ListConverter()
        self.imgc = ImgConverter()
        self.mdtxt = ''

    def _read_soup(self):
        if self.soup is None:
            raise TypeError("soup is NoneType")
        self.title = self.soup.header.h1.string
        self.content = self.soup.find('section', attrs={'data-field': 'body'})

    def read_from_file(self, fname):
        with open(fname) as fp:
            self.soup = BeautifulSoup(fp)
        self._read_soup()

    def read_from_str(self, data):
        self.soup = BeautifulSoup(data)
        self._read_soup()

    def convert(self):
        markdown = []
        subtitle = self.soup.find('section', attrs={'data-field': 'subtitle'})
        if subtitle:
            markdown.append(self.stc.convert(subtitle))
        for section in self.content.children:
            if isinstance(section, bs4.element.Tag):
                for div in section:
                    # section-divider which has <br>
                    if 'section-divider' in div.attrs.get('class'):
                        markdown.append('------%s' % self.pc.newline)
                    elif 'section-inner' in div.attrs.get('class'):
                        markdown.extend(self._convert(div))
        self.mdtxt = ''.join(markdown)

    def _convert(self, div):
        markdown = []
        for child in div.children:
            tag_name = getattr(child, 'name')
            if tag_name == 'p':
                md = self.pc.convert(child)
            elif tag_name in ('h2', 'h3'):
                md = self.hc.convert(child)
            elif tag_name == 'blockquote':
                md = self.bqc.convert(child)
            elif tag_name in ('ol', 'ul'):
                md = self.lc.convert(child)
            elif tag_name == 'figure':
                md = self.imgc.convert(child)
            else:
                logging.debug("%s didn't handle!" % tag_name)
                md = ''
            markdown.append(md)
        return markdown

    def save_to_file(self):
        with open(self.title + '.md', 'wb') as f:
            f.write(self.mdtxt.encode('utf-8'))
        logging.info('Converted ' + self.title)


def open_zip_file(zip_name):
    if not is_zipfile(zip_name):
        raise TypeError("File is not a zip file")
    with ZipFile(zip_name) as z:
        for i in z.infolist():
            yield z.read(i)


def convert_from_file(fname):
    md = Factory()
    md.read_from_file(fname)
    md.convert()
    md.save_to_file()


def convert_from_zip(zip_name):
    data = open_zip_file(zip_name)
    for i in data:
        md = Factory()
        md.read_from_str(i)
        md.convert()
        md.save_to_file()


def convert(args=sys.argv[1:]):
    debug = False
    logging.basicConfig(
        level=logging.DEBUG if debug else logging.INFO,
        format='%(levelname)s - %(asctime)s %(message)s',
        datefmt='[%b %d %H:%M:%S]')
    if args[0] in ('-h', '--help'):
        print """usage:
python convert.py [zip-file]/[html-file]
"""
    elif 'zip' in args[0]:
        convert_from_zip(args[0])
    else:
        logging.debug('filename: ' + ' '.join(args))
        map(convert_from_file, args)
    exit(0)


if __name__ == '__main__':
    convert()
