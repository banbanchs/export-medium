convert [medium](https://medium.com/) export content to markdown

*only support medium tag(p, h1, h2, a, blockquote, b, i, ol, ul, subtitle, img)*

## usage

    python convert.py [zip-file]/[html-file]

## requirement

- beautifulsoup4

## TODO

- Detail improve
- Improve performance
- ~~Directly convert from zip file~~
