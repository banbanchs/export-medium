#!/usr/bin/python2
#coding=utf-8

from convert import *


pc = PConverter()
bqc = BlockquoteConverter()
hc = HeadingConverter()
lc = ListConverter()
imgc = ImgConverter()
stc = SubtitleConverter()


def test_tag_p():
    soup = BeautifulSoup("<p>Test</p>")
    assert pc.convert(soup.p) == "Test" + pc.newline


def test_tag_p_with_bold():
    soup = BeautifulSoup("<p>Test <b>one</b></p>")
    assert pc.convert(soup.p) == "Test **one**" + pc.newline


def test_tag_p_with_italic():
    soup = BeautifulSoup("<p>Test <i>one</i></p>")
    assert pc.convert(soup.p) == "Test *one*" + pc.newline


def test_tag_p_with_hyperlink():
    soup = BeautifulSoup("<p>Test <a href='http://www.baidu.com'>One</a></p>")
    assert pc.convert(soup.p) == "Test [One](http://www.baidu.com)" + pc.newline


def test_tag_block_quote():
    soup = BeautifulSoup('<blockquote>This is a blockquote</blockquote>')
    assert bqc.convert(soup.blockquote) == "> This is a blockquote" + bqc.newline


def test_tag_block_quote_with_italic():
    soup = BeautifulSoup('<blockquote>This is a <i>blockquote</i></blockquote>')
    assert bqc.convert(soup.blockquote) == "> This is a *blockquote*" + bqc.newline


def test_tag_block_quote_with_bold():
    soup = BeautifulSoup('<blockquote>This is a <b>blockquote</b></blockquote>')
    assert bqc.convert(soup.blockquote) == "> This is a **blockquote**" + bqc.newline


def test_tag_block_quote_with_hyperlink():
    soup = BeautifulSoup('<blockquote>This is a <a href="http://www.baidu.com">blockquote</a></blockquote>')
    assert bqc.convert(soup.blockquote) == "> This is a [blockquote](http://www.baidu.com)" + bqc.newline


def test_tag_block_quote_with_bold_and_italic():
    soup = BeautifulSoup('<blockquote><i>This</i> is a <b>blockquote</b></blockquote>')
    assert bqc.convert(soup.blockquote) == "> *This* is a **blockquote**" + bqc.newline


def test_tag_h1():
    soup = BeautifulSoup('<h2>Hello world</h2>')
    assert hc.convert(soup.h2) == "# Hello world" + hc.newline


def test_tag_h2():
    soup = BeautifulSoup('<h3>Hello world</h3>')
    assert hc.convert(soup.h3) == "## Hello world" + hc.newline


def test_tag_h1_italic():
    soup = BeautifulSoup('<h2>Hello <i>world</i></h2>')
    assert hc.convert(soup.h2) == "# Hello *world*" + hc.newline


def test_tag_h2_italic():
    soup = BeautifulSoup('<h3>Hello <i>world</i></h3>')
    assert hc.convert(soup.h3) == "## Hello *world*" + hc.newline


def test_tag_ul():
    soup = BeautifulSoup('<ul class="post-list"><li name="d94f">One</li><li name="6811">Two</li><li>Three</li></ul>')
    assert lc.convert(soup.ul) == "- One%(newline)s- Two%(newline)s- Three%(newline)s" % {'newline': hc.newline}


def test_tag_ol():
    soup = BeautifulSoup('<ol class="post-list"><li name="d94f">One</li><li name="6811">Two</li><li>Three</li></ol>')
    assert lc.convert(soup.ol) == "1. One%(newline)s2. Two%(newline)s3. Three%(newline)s" % {'newline': hc.newline}


def test_tag_img_with_figcaption():
    soup = BeautifulSoup(
        '<figure name="c717"><img data-height="702" data-image-id="1*XJDEmbJGIwqRHDtSucIH2A.jpeg" data-width="636"'
        ' height="702" src="https://d262ilb51hltx0.cloudfront.net/max/800/1*XJDEmbJGIwqRHDtSucIH2A.jpeg" width="636"/>'
        '<figcaption class="image-caption">desc</figcaption></figure>')
    assert imgc.convert(soup.figure) == '![desc](https://d262ilb51hltx0.cloudfront.net/max/' \
                                        '800/1*XJDEmbJGIwqRHDtSucIH2A.jpeg "desc")' + imgc.newline


def test_tag_img_without_figcaption():
    soup = BeautifulSoup(
        '<figure name="c717"><img data-height="702" data-image-id="1*XJDEmbJGIwqRHDtSucIH2A.jpeg" data-width="636"'
        ' height="702" src="https://d262ilb51hltx0.cloudfront.net/max/800/1*XJDEmbJGIwqRHDtSucIH2A.jpeg" width="636"/>'
        '</figure>')
    assert imgc.convert(soup.figure) == '![](https://d262ilb51hltx0.cloudfront.net/max/' \
                                        '800/1*XJDEmbJGIwqRHDtSucIH2A.jpeg)' + imgc.newline


def test_subtitle():
    soup = BeautifulSoup(
        """<section data-field="subtitle">
This is a subtitle
</section>"""
    )
    assert stc.convert(soup.section) == "This is a subtitle%s" % stc.newline

